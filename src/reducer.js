import { findAllByDisplayValue } from "@testing-library/react";

export const initialState = {
    user: null,
    playlist: [],
    spotify: null,
    discover_weekly: null,
    top_artists: null,
    item: null,
    playing: false,
    //Remove after finished develop
    // token: 'BQBaqqacvMQ7vRB4M2Y06SyJlwYjuadIqrFVsJkiZXfalzTm78QcOrfslWYCRadZc_hX-pY-5tw9Pwf9gbreSK1UOsh7Ekv62sNkvil5B3pefC9Kv6jLx5dTUww8UUDb6DzofrY7agh38Z603BgHGVtD0kEhavkGFrVAXzg_-_NQ9Cle',
};

const reducer = (state, action) => {
    console.log(action);

    // Action -> type, [payload]

    switch(action.type){
        case "SET_USER":
      return {
        ...state,
        user: action.user,
      };

    case "SET_PLAYING":
      return {
        ...state,
        playing: action.playing,
      };

    case "SET_ITEM":
      return {
        ...state,
        item: action.item,
      };

    case "SET_DISCOVER_WEEKLY":
      return {
        ...state,
        discover_weekly: action.discover_weekly,
      };

    case "SET_TOP_ARTISTS":
      return {
        ...state,
        top_artists: action.top_artists,
      };

    case "SET_TOKEN":
      return {
        ...state,
        token: action.token,
      };

    case "SET_SPOTIFY":
      return {
        ...state,
        spotify: action.spotify,
      };

    case "SET_PLAYLISTS":
      return {
        ...state,
        playlists: action.playlists,
      };
    default:
      return state;
    }
};

export default reducer;